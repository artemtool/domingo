﻿using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Managers;
using Domingo.CoreAPI.Models;
using Domingo.CoreAPI.SignalR.Hubs;
using Domingo.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Domingo.CoreAPI
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			TokenValidationHandler handler = new TokenValidationHandler(Configuration);
			handler.Configure(services);

			services.AddDbContext<CoreContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Default")));
			services.AddTransient<IAutocompleteManager, AutocompleteManager>();
			services.AddTransient<IPropertyManager, PropertyManager>();
			services.AddTransient<IBookingManager, BookingManager>();
			services.AddTransient<IFileManager, FileManager>();
			services.AddTransient<IMessangerManager, MessangerManager>();
			services.AddTransient<ILiveUpdatesManager, LiveUpdatesManager>();

			services.AddCors(o => o.AddPolicy("AllowAnyOrigin", builder =>
			{
				builder.WithOrigins("http://localhost:3000")
					   .AllowAnyMethod()
					   .AllowAnyHeader()
					   .AllowCredentials();
			}));
			services.AddMvc()
				.AddJsonOptions(options =>
				{
					options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
					options.SerializerSettings.ContractResolver
						= new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
				});

			services.AddSingleton<IConfiguration>(Configuration);
			services.AddSignalR();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseAuthentication();
			app.UseCors("AllowAnyOrigin");
            app.UseSignalR(routes =>
            {
                routes.MapHub<LiveUpdatesHub>("/liveUpdatesHub");
            });
			app.UseMvc();
		}
	}
}
