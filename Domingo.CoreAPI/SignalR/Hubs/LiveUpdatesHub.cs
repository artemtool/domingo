﻿using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.SignalR.Hubs
{
	[Authorize]
	public class LiveUpdatesHub : Hub
	{
		private IBookingManager _bookingManager;
		private IMessangerManager _messangerManager;
		private ILiveUpdatesManager _liveUpdatesManager;

		public LiveUpdatesHub(
			IBookingManager bookingManager, 
			IMessangerManager messangerManager,
			ILiveUpdatesManager liveUpdatesManager
			)
		{
			this._bookingManager = bookingManager;
			this._messangerManager = messangerManager;
			this._liveUpdatesManager = liveUpdatesManager;
		}


		public override Task OnConnectedAsync()
		{
			string id = this.Context.User.Claims
					.Where(c => c.Type == "Id")
					.FirstOrDefault()
					.Value;
			Guid userIdFromClaims = new Guid(id);

			this._liveUpdatesManager.AddHubConnection(Context.ConnectionId, userIdFromClaims);

			List<Guid> connectedBookingIds = this._bookingManager.GetAllUserConnectedBookingIds(userIdFromClaims);
			foreach (var bookingId in connectedBookingIds)
			{
				this.AddHubGroupConnection(Context.ConnectionId, bookingId);
			}

			List<NewPropertyBookings> newBookings = this._bookingManager.GetNewPropertyBookings(userIdFromClaims);

			Clients.Caller
				.SendAsync("ReceiveOnConnectedUpdates", new OnConnectedUpdates
				{
					NewPropertyBookings = newBookings
				});

			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{

			this._liveUpdatesManager.DeleteHubConnection(Context.ConnectionId);

			// SignalR handles Group disconnecting on client disconnecting automatically.
			return base.OnConnectedAsync();
		}

		public async Task SendMessage(Message message)
		{
			string id = this.Context.User.Claims
					.Where(c => c.Type == "Id")
					.FirstOrDefault()
					.Value;
			Guid userIdFromClaims = new Guid(id);
			bool isAccessToChatAllowed = userIdFromClaims == message.SenderId &&
				this._liveUpdatesManager.IsAccessToGroupAllowed(message.PropertyBookingId, userIdFromClaims);

			if (isAccessToChatAllowed)
			{
				bool isConnectedToGroup = this._liveUpdatesManager.IsConnectedToGroup(message.PropertyBookingId, Context.ConnectionId);
				if (!isConnectedToGroup)
				{
					this.AddHubGroupConnection(Context.ConnectionId, message.PropertyBookingId);
				}
				PropertyBooking booking = this._bookingManager.GetById(message.PropertyBookingId);
				Guid anotherPartiesId = booking.BookerId == message.SenderId
					? booking.Property.Owner.Id
					: booking.Booker.Id;
				List<string> anotherPartiesConnectionIds = this._liveUpdatesManager.GetUserConnectionIds(anotherPartiesId);
				foreach (var connectionId in anotherPartiesConnectionIds)
				{
					bool addConnection = !this._liveUpdatesManager.IsConnectedToGroup(message.PropertyBookingId, connectionId);
					if (addConnection)
					{
						this.AddHubGroupConnection(connectionId, message.PropertyBookingId);
					}
				}

				Message updatedMessage = this._messangerManager.SaveMessage(message);

				await Clients
					.Group(message.PropertyBookingId.ToString())
					.SendAsync("ReceiveMessage", updatedMessage);
			}
		}

		private void AddHubGroupConnection(string connectionId, Guid groupId)
		{
			this._liveUpdatesManager.AddHubGroupConnection(groupId, connectionId);
			Groups.AddToGroupAsync(connectionId, groupId.ToString());
		}
	}
}
