﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Enums
{
	public enum PropertyType
	{
		Apartment = 0,
		Villa = 1,
		HotelRoom = 2
	}
}
