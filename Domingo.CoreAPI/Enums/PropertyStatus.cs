﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Enums
{
	public enum PropertyStatus
	{
		Active = 0,
		Inactive = 1,
		WaitingForModeration = 2
	}
}
