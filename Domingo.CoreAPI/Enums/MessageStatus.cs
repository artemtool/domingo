﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Enums
{
	public enum MessageStatus
	{
		Sending = 0,
		Sent = 1,
		Read = 2,
		Error = 3
	}
}
