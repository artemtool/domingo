﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Domingo.CoreAPI.Controllers
{
    [Route("[controller]")]
    public class AutocompleteController : Controller
    {

		private IAutocompleteManager _autocompleteManager;

		public AutocompleteController(IAutocompleteManager autocompleteManager)
		{
			this._autocompleteManager = autocompleteManager;
		}

		[HttpGet("regions")]
		public List<Region> GetRegions()
        {
            return this._autocompleteManager.GetRegions();
        }
    }
}
