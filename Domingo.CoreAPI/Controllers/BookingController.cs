﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using Domingo.CoreAPI.SignalR.Hubs;
using Domingo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Domingo.CoreAPI.Controllers
{
	[Route("[controller]")]
	public class BookingController : Controller
	{
		private IBookingManager _bookingManager;
		private IPropertyManager _propertyManager;

		public BookingController(IBookingManager bookingManager, IPropertyManager propertyManager)
		{
			this._bookingManager = bookingManager;
			this._propertyManager = propertyManager;
		}

		[Authorize]
		[HttpPost("add")]
		public IActionResult AddBooking([FromBody] BookPropertyRequest request)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			PropertyBooking booking = this._bookingManager.AddBooking(request, new Guid(userId));

			if (booking == null)
			{
				return BadRequest();
			}
			else
			{
				this._bookingManager.NotifyOwnerAboutNewBooking(booking);
				return Ok(booking);
			}
		}

		[Authorize]
		[HttpPost("list/my")]
		public List<PropertyBooking> GetUserBookings([FromBody]PageableListParameters parameters)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			return this._bookingManager.GetUserBookings(parameters, new Guid(userId));
		}

		[Authorize]
		[HttpPost("list/incoming")]
		public IActionResult GetIncomingBookings([FromBody]IncomingBookingsParameters parameters)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			User owner = this._propertyManager.GetPropertyOwner(parameters.PropertyId);

			if (owner == null)
			{
				return BadRequest();
			}
			else if (owner.Id.ToString() != userId)
			{
				return Forbid();
			}
			else
			{
				List<PropertyBooking> bookings = this._bookingManager.GetIncomingBookings(parameters);
				this._bookingManager.MarkNewPropertyBookingsAsSeen(bookings, parameters.PropertyId);

				return Ok(bookings);
			}
		}
	}
}
