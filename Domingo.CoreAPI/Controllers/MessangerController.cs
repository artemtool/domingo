﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using Domingo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Domingo.CoreAPI.Controllers
{
	[Route("[controller]")]
	public class MessangerController : Controller
	{
		private IBookingManager _bookingManager;
		private IMessangerManager _messangerManager;
		ILiveUpdatesManager _liveUpdatesManager;

		public MessangerController(
			IBookingManager bookingManager,
			IMessangerManager messangerManager,
			ILiveUpdatesManager liveUpdatesManager
			)
		{
			this._bookingManager = bookingManager;
			this._messangerManager = messangerManager;
			this._liveUpdatesManager = liveUpdatesManager;
		}

		[Authorize]
		[HttpPost("message/list")]
		public IActionResult GetMessages([FromBody]FetchMessagesRequest request)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			bool isAccessAllowed = this._liveUpdatesManager.IsAccessToGroupAllowed(request.BookingId, new Guid(userId));

			if (isAccessAllowed)
			{
				return Ok(this._messangerManager.GetMessages(request));
			}
			else
			{
				return Forbid();
			}
		}
	}
}
