﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Domingo.CoreAPI.Controllers
{
	public class ImageController : Controller
	{
		private string _propertyImageTemplateUrl;
		private string _userImageTemplateUrl;

		public ImageController(IConfiguration configuration)
		{
			this._propertyImageTemplateUrl = configuration["StorePathTemplates:PropertyImage"];
			this._userImageTemplateUrl = configuration["StorePathTemplates:UserImage"];
		}

		[HttpGet("property/{propertyId}/image/{imageName}")]
		public async Task<IActionResult> GetPropertyImage([FromRoute] string propertyId, [FromRoute] string imageName)
		{
			string pathToImagesStore = String.Format(this._propertyImageTemplateUrl, propertyId);
			string imagePath = String.Format("{0}\\{1}", pathToImagesStore, imageName);
			try
			{
				var image = System.IO.File.OpenRead(imagePath);
				return File(image, "image/jpeg");
			}
			catch (Exception ex)
			{
				return NotFound();
			}
		}

		[HttpGet("user/{userId}/image/{imageName}")]
		public async Task<IActionResult> GetUserImage([FromRoute] string userId, [FromRoute] string imageName)
		{
			string pathToImagesStore = String.Format(this._userImageTemplateUrl, userId);
			string imagePath = String.Format("{0}\\{1}", pathToImagesStore, imageName);
			try
			{
				var image = System.IO.File.OpenRead(imagePath);
				return File(image, "image/jpeg");
			}
			catch (Exception ex)
			{
				return NotFound();
			}
		}
	}
}
