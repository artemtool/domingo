﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using Domingo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Domingo.CoreAPI.Controllers
{
	[Route("[controller]")]
	public class PropertyController : Controller
	{
		private IPropertyManager _propertyManager;

		public PropertyController(IPropertyManager propertyManager)
		{
			this._propertyManager = propertyManager;
		}

		[HttpPost("search")]
		public List<PropertyCard> Search([FromBody]SearchParameters parameters)
		{
			return this._propertyManager.Search(parameters);
		}

		[HttpGet("{id}")]
		public IActionResult GetById([FromRoute] Guid id)
		{
			Property property = this._propertyManager.GetById(id);
			if (property != null)
			{
				return Ok(property);
			}
			else
			{
				return NotFound();
			}
		}

		[Authorize]
		[HttpGet("{id}/edit")]
		public IActionResult GetByIdForEdit([FromRoute] Guid id)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			User owner = this._propertyManager.GetPropertyOwner(id);

			if (owner == null)
			{
				return BadRequest();
			}
			else if (owner.Id.ToString() != userId)
			{
				return Forbid();
			}
			else
			{
				Property property = this._propertyManager.GetById(id);
				if (property != null)
				{
					return Ok(property);
				}
				else
				{
					return NotFound();
				}
			}
		}

		[HttpGet("card/{id}")]
		public IActionResult GetCardById([FromRoute] Guid id)
		{
			PropertyCard property = this._propertyManager.GetCardById(id);
			if (property != null)
			{
				return Ok(property);
			}
			else
			{
				return NotFound();
			}
		}

		[Authorize]
		[HttpPost("publish")]
		public async Task<Property> Publish(PublishPropertyRequest request)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			Property property = await this._propertyManager.PublishProperty(request, new Guid(userId));
			return property;
		}

		[Authorize]
		[HttpPost("update")]
		public async Task<IActionResult> Update(UpdatePropertyRequest request)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			User owner = this._propertyManager.GetPropertyOwner(new Guid(request.PropertyId));

			if (owner == null)
			{
				return BadRequest();
			}
			else if (owner.Id.ToString() != userId)
			{
				return Forbid();
			}
			else
			{
				Property property;
				try
				{
					property = await this._propertyManager.UpdateProperty(request, new Guid(userId));
				}
				catch (Exception ex)
				{
					return BadRequest();
				}
				return Ok(property);
			}
		}

		[Authorize]
		[HttpPost("list/published")]
		public List<PropertyCard> GetPublished([FromBody]PageableListParameters parameters)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			return this._propertyManager.GetPublished(parameters, new Guid(userId));
		}

		[Authorize]
		[HttpGet("list/published/suggestions/{address}")]
		public List<PropertyCard> GetPublishedByAddress([FromRoute] string address)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			return this._propertyManager.GetPublishedByAddress(address, new Guid(userId));
		}

		[Authorize]
		[HttpPost("changeStatus")]
		public IActionResult ChangeStatus([FromBody]ChangePropertyStatusRequest request)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			User owner = this._propertyManager.GetPropertyOwner(new Guid(request.PropertyId));

			if (owner == null)
			{
				return BadRequest();
			}
			else if (owner.Id.ToString() != userId)
			{
				return Forbid();
			}
			else
			{
				bool isChanged = this._propertyManager.ChangeStatus(request);

				if (isChanged)
				{
					return Ok();
				}
				else
				{
					return BadRequest();
				}
			}
		}

		[Authorize]
		[HttpDelete("{id}")]
		public IActionResult Delete([FromRoute]string id)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;

			User owner = this._propertyManager.GetPropertyOwner(new Guid(id));

			if (owner == null)
			{
				return BadRequest();
			}
			else if (owner.Id.ToString() != userId)
			{
				return Forbid();
			}
			else
			{
				bool areThereAnyBookings = this._propertyManager.AreThereAnyBookings(id);

				if (areThereAnyBookings) {
					return Forbid();
				}

				bool isDeleted = this._propertyManager.Delete(id);

				if (isDeleted)
				{
					return Ok();
				}
				else
				{
					return BadRequest();
				}
			}
		}
	}
}
