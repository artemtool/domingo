﻿using Domingo.CoreAPI.Models;
using Domingo.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Domingo.CoreAPI
{
    public static class DbInitializer
    {

        public static void Initialize(CoreContext context)
        {
            context.Database.EnsureCreated();

			InitializeUsers(context);
			InitializeAutocompleteData(context);
			InitializePropertyData(context);
		}

		public static void InitializeUsers(CoreContext context)
		{
			if (context.Users.Any())
			{
				return;
			}

			List<User> users = new List<User>();
			using (StreamReader r = new StreamReader("Data/users.json"))
			{
				string json = r.ReadToEnd();
				users = JsonConvert.DeserializeObject<List<User>>(json);
			}
			foreach (var user in users)
			{
				user.Password = "55085deadcfa54f324143d32774cd899a64b189aae72763976893bc76ed0ee31"; // JsonIgnore
				context.Users.Add(user);
			}
			context.SaveChanges();
		}

		public static void InitializeAutocompleteData(CoreContext context)
		{
			if (context.Regions.Any())
			{
				return;
			}

			List<Region> regions = new List<Region>();
			using (StreamReader r = new StreamReader("Data/regions.json"))
			{
				string json = r.ReadToEnd();
				regions = JsonConvert.DeserializeObject<List<Region>>(json);
			}
			foreach (var region in regions)
			{
				context.Regions.Add(region);
			}
			context.SaveChanges();
		}

		public static void InitializePropertyData(CoreContext context)
		{
			if (context.Properties.Any())
			{
				return;
			}

			List<Property> properties = new List<Property>();
			using (StreamReader r = new StreamReader("Data/properties.json"))
			{
				string json = r.ReadToEnd();
				properties = JsonConvert.DeserializeObject<List<Property>>(json);
			}
			foreach (var property in properties)
			{
				context.Properties.Add(property);
			}
			context.SaveChanges();
		}
	}
}
