﻿using Domingo.CoreAPI.Enums;
using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Managers
{
	public class MessangerManager: IMessangerManager
	{
		private CoreContext _context;

		public MessangerManager(CoreContext context)
		{
			this._context = context;
		}

		public Message SaveMessage(Message message)
		{
			message.Date = DateTime.Now;
			message.Status = MessageStatus.Sent;
			this._context.BookingMessages.Add(message);
			this._context.SaveChanges();

			return message;
		}

		public List<Message> GetMessages(FetchMessagesRequest request)
		{
			var isDesc = request.ListParameters.IsDesc;
			var orderBy = typeof(Message).GetProperty(request.ListParameters.OrderBy);

			IQueryable<Message> queryable = this._context.BookingMessages
				.Where(b => b.PropertyBookingId == request.BookingId);

			IQueryable<Message> orderedQueryable = null;
			if (isDesc)
			{
				orderedQueryable = queryable
					.OrderByDescending(m => orderBy.GetValue(m, null));
			}
			else
			{
				orderedQueryable = queryable
					.OrderBy(m => orderBy.GetValue(m, null));
			}

			List<Message> result = orderedQueryable.Skip((request.ListParameters.Page - 1) * request.ListParameters.PerPageAmount)
				.Take(request.ListParameters.PerPageAmount)
				.ToList();
			result.Reverse();

			return result;
		}
	}
}
