﻿using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Managers
{
	public class AutocompleteManager: IAutocompleteManager
	{
		private CoreContext _context;

		public AutocompleteManager(CoreContext context)
		{
			this._context = context;
		}

		public List<Region> GetRegions() {
			return this._context.Regions
				.Include(region => region.Cities)
				.ToList();
		}
	}
}
