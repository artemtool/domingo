﻿using Domingo.CoreAPI.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Managers
{
	public class FileManager : IFileManager
	{
		public async Task<bool> SaveFormFiles(string path, List<IFormFile> files)
		{
			bool areFilesSaved = true;
			if (files == null || files.Count == 0)
			{
				areFilesSaved = false;
			}
			else
			{
				try
				{
					Directory.CreateDirectory(path);

					foreach (var file in files)
					{
						if (file.Length > 0)
						{
							var filePath = Path.Combine(path, file.FileName);
							using (var fileStream = new FileStream(filePath, FileMode.CreateNew))
							{
								await file.CopyToAsync(fileStream);
							}
						}
					}
				}
				catch (Exception ex)
				{
					areFilesSaved = false;
					Directory.Delete(path, true);
				}
			}

			return areFilesSaved;
		}

		public List<string> GetAllFileNames(string dirPath)
		{
			List<string> fileNames = new List<string>();

			try
			{
				DirectoryInfo d = new DirectoryInfo(dirPath);
				FileInfo[] files = d.GetFiles();


				foreach (var file in files)
				{
					fileNames.Add(file.Name);
				}
			}
			catch (Exception ex)
			{

			}

			return fileNames;
		}

		public bool DeleteFiles(string path, List<string> fileNames)
		{
			bool areFilesDeleted = true;
			if (fileNames == null || fileNames.Count == 0)
			{
				areFilesDeleted = false;
			}
			else
			{
				try
				{
					foreach (var name in fileNames)
					{
						var filePath = Path.Combine(path, name);
						File.Delete(filePath);
					}
				}
				catch (Exception ex)
				{
					areFilesDeleted = false;
				}
			}

			return areFilesDeleted;
		}
	}
}
