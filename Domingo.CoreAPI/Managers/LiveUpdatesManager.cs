﻿using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Managers
{
	public class LiveUpdatesManager : ILiveUpdatesManager
	{
		private CoreContext _context;

		public LiveUpdatesManager(CoreContext context)
		{
			this._context = context;
		}

		public bool IsAccessToGroupAllowed(Guid groupId, Guid userId)
		{
			return this._context.PropertyBookings
				.Where(b => b.Id == groupId && (b.BookerId == userId || b.Property.Owner.Id == userId))
				.Any();
		}

		public bool IsConnectedToGroup(Guid groupId, string connectionId)
		{
			return this._context.HubConnections
				.Where(c => c.ConnectionId == connectionId && c.GroupConnections.Any(gc => gc.GroupId == groupId))
				.Any();
		}

		public void AddHubGroupConnection(Guid groupId, string connectionId)
		{
			this._context.HubGroupConnections
				.Add(new HubGroupConnection
				{
					GroupId = groupId,
					HubConnectionId = connectionId
				});
			this._context.SaveChanges();
		}

		public void AddHubConnection(string connectionId, Guid userId)
		{
			this._context.HubConnections
				.Add(new HubConnection
				{
					ConnectionId = connectionId,
					UserId = userId
				});
			this._context.SaveChanges();
		}

		public void DeleteHubConnection(string connectionId)
		{
			HubConnection connection = this._context.HubConnections
				.Where(c => c.ConnectionId == connectionId)
				.FirstOrDefault();

			this._context.HubConnections.Remove(connection);
			this._context.SaveChanges();
		}

		public List<string> GetUserConnectionIds(Guid userId)
		{
			return this._context.HubConnections
				.Where(c => c.UserId == userId)
				.Select(c => c.ConnectionId)
				.ToList();
		}
	}
}
