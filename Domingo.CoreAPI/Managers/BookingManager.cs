﻿using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using Domingo.CoreAPI.SignalR.Hubs;
using Domingo.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Managers
{
	public class BookingManager : IBookingManager
	{
		private CoreContext _context;
		private ILiveUpdatesManager _liveUpdatesManager;
		private IHubContext<LiveUpdatesHub> _liveUpdatesHub;

		public BookingManager(
			CoreContext context,
			ILiveUpdatesManager liveUpdatesManager,
			IHubContext<LiveUpdatesHub> liveUpdatesHub
			)
		{
			this._context = context;
			this._liveUpdatesManager = liveUpdatesManager;
			this._liveUpdatesHub = liveUpdatesHub;
		}

		public PropertyBooking AddBooking(BookPropertyRequest request, Guid userId)
		{
			User guest = this._context.Users
				.Where(u => u.Id == userId)
				.FirstOrDefault();
			Property property = this._context.Properties
				.Where(p => p.Id == request.PropertyId)
				.Include(p => p.City)
				.Include(p => p.Owner)
				.FirstOrDefault();

			if (property != null)
			{
				PropertyBooking booking = new PropertyBooking()
				{
					CheckInDate = request.CheckInDate,
					CheckOutDate = request.CheckOutDate,
					Comments = request.Comments,
					BookerId = guest != null ? guest.Id : Guid.Empty,
					GuestName = request.GuestName,
					GuestPhoneNumber = request.GuestPhoneNumber,
					GuestsAmount = request.GuestsAmount,
					DailyPrice = property.DailyPrice,
					PropertyId = property.Id,
					PropertyAddress = string.Format("{0}, {1}", property.City.Name, property.Address),
					PropertyOwnerName = string.Format("{0} {1}", property.Owner.Name, property.Owner.Surname),
					PropertyOwnerPhone = string.Format("{0} {1} {2}", property.Owner.PhoneNumber1, property.Owner.PhoneNumber2, property.Owner.PhoneNumber3),
					PropertyMainImageName = property.MainImageName,
					WasSeenByOwner = false
				};

				this._context.PropertyBookings.Add(booking);
				this._context.SaveChanges();

				return booking;
			}
			else
			{
				return null;
			}
		}

		public List<PropertyBooking> GetUserBookings(PageableListParameters parameters, Guid userId)
		{
			return this._context.PropertyBookings
				.Where(b => b.BookerId == userId)
				.Skip((parameters.Page - 1) * parameters.PerPageAmount)
				.Take(parameters.PerPageAmount)
				.ToList();
		}

		public List<PropertyBooking> GetIncomingBookings(IncomingBookingsParameters parameters)
		{
			return this._context.Properties
				.Where(p => p.Id == parameters.PropertyId)
				.Include(p => p.Bookings)
				.FirstOrDefault()
				.Bookings
				.AsQueryable()
				.Skip((parameters.ListParameters.Page - 1) * parameters.ListParameters.PerPageAmount)
				.Take(parameters.ListParameters.PerPageAmount)
				.ToList();
		}

		public List<Guid> GetAllUserConnectedBookingIds(Guid userId)
		{
			return this._context.PropertyBookings
				.Where(b => b.BookerId == userId || b.Property.Owner.Id == userId)
				.Select(b => b.Id)
				.ToList();
		}

		public PropertyBooking GetById(Guid id)
		{
			return this._context.PropertyBookings
				.Where(b => b.Id == id)
				.Include(b => b.Booker)
				.Include(b => b.Property)
				.ThenInclude(p => p.Owner)
				.FirstOrDefault();
		}

		public void NotifyOwnerAboutNewBooking(PropertyBooking booking)
		{
			Guid ownerId = this._context.Properties
				.Where(p => p.Id == booking.PropertyId)
				.Select(p => p.Owner.Id)
				.FirstOrDefault();

			List<string> ownerConnectionIds = this._liveUpdatesManager.GetUserConnectionIds(ownerId);
			this._liveUpdatesHub.Clients
				.Clients(ownerConnectionIds)
				.SendAsync("ReceiveBooking", JsonConvert.SerializeObject(booking, new JsonSerializerSettings {
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
					ContractResolver = new CamelCasePropertyNamesContractResolver()
				}));
		}

		public void MarkNewPropertyBookingsAsSeen(List<PropertyBooking> bookings, Guid propertyId)
		{
			if (bookings.Count == 0) {
				return;
			}

			foreach (var booking in bookings) {
				booking.WasSeenByOwner = true;
			}
			this._context.PropertyBookings.UpdateRange(bookings);
			this._context.SaveChanges();

			Guid ownerId = this._context.Properties
				.Where(p => p.Id == propertyId)
				.Select(p => p.Owner.Id)
				.FirstOrDefault();

			List<string> ownerConnectionIds = this._liveUpdatesManager.GetUserConnectionIds(ownerId);
			this._liveUpdatesHub.Clients
				.Clients(ownerConnectionIds)
				.SendAsync("MarkNewPropertyBookingsAsSeen", propertyId);
		}

		public List<NewPropertyBookings> GetNewPropertyBookings(Guid userId)
		{
			return this._context.Properties
				.Where(p => p.Owner.Id == userId)
				.Select(p => new NewPropertyBookings
				{
					PropertyId = p.Id,
					PropertyAddress = string.Format("{0}, {1}", p.City.Name, p.Address),
					Count = p.Bookings.Count(b => !b.WasSeenByOwner)
				})
				.Where(b => b.Count > 0)
				.ToList();
		}
	}
}
