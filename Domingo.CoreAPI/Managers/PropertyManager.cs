﻿using Domingo.CoreAPI.Enums;
using Domingo.CoreAPI.Interfaces;
using Domingo.CoreAPI.Models;
using Domingo.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Managers
{
	public class PropertyManager : IPropertyManager
	{
		private CoreContext _context;
		private IFileManager _fileManager;
		private string _propertyStorePathTemplate;
		private string _propertyImageStorePathTemplate;
		private string _propertyImageTemplateUrl;

		public PropertyManager(CoreContext context, IFileManager fileManager, IConfiguration configuration)
		{
			this._context = context;
			this._fileManager = fileManager;
			this._propertyStorePathTemplate = configuration["StorePathTemplates:Property"];
			this._propertyImageStorePathTemplate = configuration["StorePathTemplates:PropertyImage"];
			this._propertyImageTemplateUrl = configuration["RelativeUrlTemplates:PropertyImage"];
		}

		public List<PropertyCard> Search(SearchParameters parameters)
		{
			var isDesc = parameters.ListParameters.IsDesc;
			var orderBy = typeof(PropertyCard).GetProperty(parameters.ListParameters.OrderBy);

			IQueryable<PropertyCard> queryable = this._context.Properties
				.Where(this.SearchWith(parameters))
				.Where(this.SearchNonBooked(parameters.CheckInDate, parameters.CheckOutDate))
				.Select(p => new PropertyCard
				{
					Address = p.Address,
					Area = p.Area,
					City = p.City,
					DailyPrice = p.DailyPrice,
					Floor = p.Floor,
					Id = p.Id,
					MainImageName = p.MainImageName,
					PublishDate = p.PublishDate,
					Rooms = p.Rooms,
					Sleeps = p.Sleeps,
					Status = p.Status,
					Title = p.Title,
					Type = p.Type,
					WeeklyPrice = p.WeeklyPrice,
					OwnerId = p.Owner.Id
				});

			IQueryable<PropertyCard> orderedQueryable = null;
			if (isDesc)
			{
				orderedQueryable = queryable
					.OrderByDescending(p => orderBy.GetValue(p, null));
			}
			else
			{
				orderedQueryable = queryable
					.OrderBy(p => orderBy.GetValue(p, null));
			}

			return orderedQueryable
				.Skip((parameters.ListParameters.Page - 1) * parameters.ListParameters.PerPageAmount)
				.Take(parameters.ListParameters.PerPageAmount)
				
				.ToList();
		}

		public Property GetById(Guid id)
		{
			Property property = this._context.Properties
				.Where(p => p.Id == id)
				.Include(p => p.City)
				.FirstOrDefault();

			if (property != null)
			{
				string dirPath = string.Format(this._propertyImageStorePathTemplate, id);
				List<string> fileNames = this._fileManager.GetAllFileNames(dirPath);
				property.ImageNames = fileNames
					.Select(name => string.Format(this._propertyImageTemplateUrl, id, name))
					.ToList();
			}

			return property;
		}

		public PropertyCard GetCardById(Guid id)
		{
			return this._context.Properties
				.Where(p => p.Id == id)
				.Select(p => new PropertyCard
				{
					Address = p.Address,
					Area = p.Area,
					City = p.City,
					DailyPrice = p.DailyPrice,
					Floor = p.Floor,
					Id = p.Id,
					MainImageName = p.MainImageName,
					PublishDate = p.PublishDate,
					Rooms = p.Rooms,
					Sleeps = p.Sleeps,
					Status = p.Status,
					Title = p.Title,
					Type = p.Type,
					WeeklyPrice = p.WeeklyPrice,
					OwnerId = p.Owner.Id
				})
				.FirstOrDefault();
		}

		public User GetPropertyOwner(Guid propertyId)
		{
			Property property = this._context.Properties
				.Where(p => p.Id == propertyId)
				.Include(p => p.Owner)
				.FirstOrDefault();

			if (property == null)
			{
				return null;
			}
			else
			{
				return property.Owner;
			}
		}

		public async Task<Property> PublishProperty(PublishPropertyRequest request, Guid userId)
		{
			Property property = new Property
			{
				Address = request.Address,
				Area = request.Area,
				DailyPrice = request.DailyPrice,
				Description = request.Description,
				Floor = request.Floor,
				HasAirConditioner = request.HasAirConditioner,
				HasBedLinen = request.HasBedLinen,
				HasComputer = request.HasComputer,
				HasDishes = request.HasDishes,
				HasHairDryer = request.HasHairDryer,
				HasIroningStaff = request.HasIroningStaff,
				HasKettle = request.HasKettle,
				HasMicrowaveOven = request.HasMicrowaveOven,
				HasParkingPlace = request.HasParkingPlace,
				HasTowels = request.HasTowels,
				HasTV = request.HasTV,
				HasWashingMachine = request.HasWashingMachine,
				HasWiFi = request.HasWiFi,
				Rooms = request.Rooms,
				Sleeps = request.Sleeps,
				Title = request.Title,
				Type = request.Type,
				WeeklyPrice = request.WeeklyPrice,
				PublishDate = DateTime.Now,
				Status = PropertyStatus.Active
			};

			property.City = this._context.Cities
				.Where(c => c.Id == new Guid(request.CityId))
				.FirstOrDefault();

			property.Owner = this._context.Users
				.Where(u => u.Id == userId)
				.FirstOrDefault();

			this._context.Properties.Add(property);

			string pathToImagesStore = String.Format(this._propertyImageStorePathTemplate, property.Id);
			bool imagesAdded = await this._fileManager.SaveFormFiles(pathToImagesStore, request.ImagesToUpload);

			if (imagesAdded)
			{
				property.MainImageName = String.Format(this._propertyImageTemplateUrl, property.Id, request.MainImageName);
			}

			this._context.SaveChanges();
			return property;
		}

		public async Task<Property> UpdateProperty(UpdatePropertyRequest request, Guid userId)
		{
			Property property = this._context.Properties
				.Where(c => c.Id == new Guid(request.PropertyId))
				.FirstOrDefault();

			property.Address = request.Address;
			property.Area = request.Area;
			property.DailyPrice = request.DailyPrice;
			property.Description = request.Description;
			property.Floor = request.Floor;
			property.HasAirConditioner = request.HasAirConditioner;
			property.HasBedLinen = request.HasBedLinen;
			property.HasComputer = request.HasComputer;
			property.HasDishes = request.HasDishes;
			property.HasHairDryer = request.HasHairDryer;
			property.HasIroningStaff = request.HasIroningStaff;
			property.HasKettle = request.HasKettle;
			property.HasMicrowaveOven = request.HasMicrowaveOven;
			property.HasParkingPlace = request.HasParkingPlace;
			property.HasTowels = request.HasTowels;
			property.HasTV = request.HasTV;
			property.HasWashingMachine = request.HasWashingMachine;
			property.HasWiFi = request.HasWiFi;
			property.Rooms = request.Rooms;
			property.Sleeps = request.Sleeps;
			property.Title = request.Title;
			property.Type = request.Type;
			property.WeeklyPrice = request.WeeklyPrice;

			property.City = this._context.Cities
				.Where(c => c.Id == new Guid(request.CityId))
				.FirstOrDefault();

			property.Owner = this._context.Users
				.Where(u => u.Id == userId)
				.FirstOrDefault();

			string pathToImagesStore = String.Format(this._propertyImageStorePathTemplate, property.Id);
			this._fileManager.DeleteFiles(pathToImagesStore, request.ImagesToDelete);
			await this._fileManager.SaveFormFiles(pathToImagesStore, request.ImagesToUpload);

			if (request.MainImageName != property.MainImageName)
			{
				if (String.IsNullOrEmpty(request.MainImageName))
				{
					property.MainImageName = null;
				}
				else
				{
					property.MainImageName = String.Format(this._propertyImageTemplateUrl, property.Id, request.MainImageName);
				}

			}

			this._context.Properties.Update(property);
			this._context.SaveChanges();

			return property;
		}

		public List<PropertyCard> GetPublished(PageableListParameters parameters, Guid userId)
		{
			return this._context.Properties
				.Where(p => p.Owner.Id == userId)
				.Skip((parameters.Page - 1) * parameters.PerPageAmount)
				.Take(parameters.PerPageAmount)
				.Select(p => new PropertyCard
				{
					Address = p.Address,
					Area = p.Area,
					City = p.City,
					DailyPrice = p.DailyPrice,
					Floor = p.Floor,
					Id = p.Id,
					MainImageName = p.MainImageName,
					PublishDate = p.PublishDate,
					Rooms = p.Rooms,
					Sleeps = p.Sleeps,
					Status = p.Status,
					Title = p.Title,
					Type = p.Type,
					WeeklyPrice = p.WeeklyPrice,
					OwnerId = p.Owner.Id
				})
				.ToList();
		}

		public List<PropertyCard> GetPublishedByAddress(string address, Guid userId)
		{
			return this._context.Properties
				.Where(p => p.Owner.Id == userId && (p.Address.Contains(address) || p.City.Name.Contains(address)))
				.Select(p => new PropertyCard
				{
					Address = p.Address,
					Area = p.Area,
					City = p.City,
					DailyPrice = p.DailyPrice,
					Floor = p.Floor,
					Id = p.Id,
					MainImageName = p.MainImageName,
					PublishDate = p.PublishDate,
					Rooms = p.Rooms,
					Sleeps = p.Sleeps,
					Status = p.Status,
					Title = p.Title,
					Type = p.Type,
					WeeklyPrice = p.WeeklyPrice,
					OwnerId = p.Owner.Id
				})
				.ToList();
		}

		public bool ChangeStatus(ChangePropertyStatusRequest request)
		{
			Property property = this._context.Properties
				.Where(p => p.Id.ToString() == request.PropertyId)
				.FirstOrDefault();

			if (property != null)
			{
				property.Status = request.PropertyStatus;
				this._context.SaveChanges();
				return true;
			}
			else
			{
				return false;
			}

		}

		public bool Delete(string propertyId)
		{
			Property property = this._context.Properties
				.Where(p => p.Id.ToString() == propertyId)
				.FirstOrDefault();

			if (property != null)
			{
				string pathToPropertyStore = String.Format(this._propertyStorePathTemplate, property.Id);
				Directory.Delete(pathToPropertyStore, true);

				this._context.Properties
					.Remove(property);
				this._context.SaveChanges();

				return true;
			}
			else
			{
				return false;
			}

		}

		public bool AreThereAnyBookings(string propertyId)
		{
			return this._context.PropertyBookings
					.Where(b => b.PropertyId.ToString() == propertyId)
					.Any();
		}

		private Expression<Func<Property, bool>> SearchWith(SearchParameters parameters)
		{
			return (property => property.City.Id == parameters.CityId &&
								property.Status == PropertyStatus.Active &&
								(property.Type == parameters.PropertyType || parameters.PropertyType == null) &&
								(property.Sleeps == parameters.Sleeps || parameters.Sleeps == null) &&
								(property.Rooms >= parameters.MinRooms || parameters.MinRooms == null) &&
								(property.Rooms <= parameters.MaxRooms || parameters.MaxRooms == null) &&
								(property.DailyPrice >= parameters.MinPrice || parameters.MinPrice == null) &&
								(property.DailyPrice <= parameters.MaxPrice || parameters.MaxPrice == null) &&
								(property.HasAirConditioner || parameters.HasAirConditioner == null) &&
								(property.HasBedLinen || parameters.HasBedLinen == null) &&
								(property.HasComputer || parameters.HasComputer == null) &&
								(property.HasDishes || parameters.HasDishes == null) &&
								(property.HasHairDryer || parameters.HasHairDryer == null) &&
								(property.HasIroningStaff || parameters.HasIroningStaff == null) &&
								(property.HasKettle || parameters.HasKettle == null) &&
								(property.HasMicrowaveOven || parameters.HasMicrowaveOven == null) &&
								(property.HasParkingPlace || parameters.HasParkingPlace == null) &&
								(property.HasTowels || parameters.HasTowels == null) &&
								(property.HasTV || parameters.HasTV == null) &&
								(property.HasWashingMachine || parameters.HasWashingMachine == null) &&
								(property.HasWiFi || parameters.HasWiFi == null));
		}

		private Expression<Func<Property, bool>> SearchNonBooked(DateTime checkInDate, DateTime checkOutDate)
		{
			return (property => !property.Bookings
			.Any(b => b.CheckInDate <= checkInDate && b.CheckOutDate >= checkOutDate || // checkIn before searchCheckIn && checkOut after searchCheckOut
					  b.CheckInDate >= checkInDate && b.CheckInDate < checkOutDate || // checkIn in the search period
					  b.CheckOutDate > checkInDate && b.CheckOutDate <= checkOutDate)); // checkOut in the search period
		}
	}
}
