﻿using System;
using System.Collections.Generic;

namespace Domingo.CoreAPI.Models
{
	public class Region
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public List<City> Cities { get; set; }
	}
}
