﻿using Domingo.CoreAPI.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class SearchParameters
	{
		public Guid CityId { get; set; }
		public DateTime CheckInDate { get; set; }
		public DateTime CheckOutDate { get; set; }
		public int? MinPrice { get; set; }
		public int? MaxPrice { get; set; }
		public int? MinRooms { get; set; }
		public int? MaxRooms { get; set; }
		public int? Sleeps { get; set; }
		public bool? HasWiFi { get; set; }
		public bool? HasParkingPlace { get; set; }
		public bool? HasTV { get; set; }
		public bool? HasAirConditioner { get; set; }
		public bool? HasBedLinen { get; set; }
		public bool? HasComputer { get; set; }
		public bool? HasDishes { get; set; }
		public bool? HasHairDryer { get; set; }
		public bool? HasIroningStaff { get; set; }
		public bool? HasKettle { get; set; }
		public bool? HasMicrowaveOven { get; set; }
		public bool? HasTowels { get; set; }
		public bool? HasWashingMachine { get; set; }
		public PropertyType? PropertyType { get; set; }
		public PageableListParameters ListParameters { get; set; }
	}
}
