﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class FetchMessagesRequest
	{
		public Guid BookingId { get; set; }
		public PageableListParameters ListParameters { get; set; }
	}
}
