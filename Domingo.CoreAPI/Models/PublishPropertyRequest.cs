﻿using Domingo.CoreAPI.Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class PublishPropertyRequest
	{
		public List<IFormFile> ImagesToUpload { get; set; }
		public string CityId { get; set; }
		public PropertyType Type { get; set; }
		public string Title { get; set; }
		public string Address { get; set; }
		public string Description { get; set; }
		public string MainImageName { get; set; }
		public int DailyPrice { get; set; }
		public int WeeklyPrice { get; set; }
		public int Rooms { get; set; }
		public int Area { get; set; }
		public int Sleeps { get; set; }
		public int Floor { get; set; }
		public bool HasWiFi { get; set; }
		public bool HasParkingPlace { get; set; }
		public bool HasTV { get; set; }
		public bool HasAirConditioner { get; set; }
		public bool HasBedLinen { get; set; }
		public bool HasComputer { get; set; }
		public bool HasDishes { get; set; }
		public bool HasHairDryer { get; set; }
		public bool HasIroningStaff { get; set; }
		public bool HasKettle { get; set; }
		public bool HasMicrowaveOven { get; set; }
		public bool HasTowels { get; set; }
		public bool HasWashingMachine { get; set; }
	}
}
