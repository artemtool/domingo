﻿using Domingo.CoreAPI.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class NewPropertyBookings
	{
		public Guid PropertyId { get; set; }
		public string PropertyAddress { get; set; }
		public int Count { get; set; }
	}
}
