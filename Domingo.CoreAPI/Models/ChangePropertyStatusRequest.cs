﻿using Domingo.CoreAPI.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class ChangePropertyStatusRequest
	{
		public string PropertyId { get; set; }
		public PropertyStatus PropertyStatus { get; set; }
	}
}
