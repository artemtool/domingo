﻿using Domingo.CoreAPI.Enums;
using Domingo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class Message
	{
		public Guid Id { get; set; }
		public Guid SenderId { get; set; }
		public Guid PropertyBookingId { get; set; }
		public DateTime Date { get; set; }
		public string Text { get; set; }
		public MessageStatus Status { get; set; }
	}
}
