﻿using Domingo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class HubGroupConnection
	{
		public Guid Id { get; set; }
		public Guid GroupId { get; set; }
		public string HubConnectionId { get; set; }
		public HubConnection HubConnection { get; set; }
	}
}
