﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class OnConnectedUpdates
	{
		public List<NewPropertyBookings> NewPropertyBookings { get; set; }
	}
}
