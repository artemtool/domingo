﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class UpdatePropertyRequest : PublishPropertyRequest
	{
		public string PropertyId { get; set; }
		public List<string> ImagesToDelete { get; set; }
	}
}
