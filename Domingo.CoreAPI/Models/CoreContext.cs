﻿using Domingo.Models;
using Microsoft.EntityFrameworkCore;

namespace Domingo.CoreAPI.Models
{
	public class CoreContext : DbContext
	{
		public DbSet<Region> Regions { get; set; }
		public DbSet<City> Cities { get; set; }
		public DbSet<Property> Properties { get; set; }
		public DbSet<PropertyBooking> PropertyBookings { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<RefreshToken> RefreshTokens { get; set; }
		public DbSet<Message> BookingMessages { get; set; }
		public DbSet<HubConnection> HubConnections { get; set; }
		public DbSet<HubGroupConnection> HubGroupConnections { get; set; }

		public CoreContext(DbContextOptions<CoreContext> options) : base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Property>()
				.HasMany(p => p.Bookings)
				.WithOne("Property")
				.OnDelete(DeleteBehavior.SetNull);

			modelBuilder.Entity<HubConnection>()
				.HasMany(c => c.GroupConnections)
				.WithOne("HubConnection")
				.OnDelete(DeleteBehavior.Cascade);
		}
	}
}
