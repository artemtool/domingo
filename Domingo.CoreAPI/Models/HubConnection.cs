﻿using Domingo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class HubConnection
	{
		[Key]
		public string ConnectionId { get; set; }
		public Guid UserId { get; set; }
		public User User { get; set; }
		public List<HubGroupConnection> GroupConnections { get; set; }
	}
}
