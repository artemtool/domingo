﻿using Domingo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domingo.CoreAPI.Models
{
	public class PropertyBooking
	{
		public Guid Id { get; set; }
		public Guid? BookerId { get; set; }
		public User Booker { get; set; }
		public Guid? PropertyId { get; set; }
		public Property Property { get; set; }
		public List<Message> Messages { get; set; }
		public DateTime CheckInDate { get; set; }
		public DateTime CheckOutDate { get; set; }
		public string GuestName { get; set; }
		public string GuestPhoneNumber { get; set; }
		public int GuestsAmount { get; set; }
		public string Comments { get; set; }
		public int DailyPrice { get; set; }
		public string PropertyMainImageName { get; set; }
		public string PropertyAddress { get; set; }
		public string PropertyOwnerName { get; set; }
		public string PropertyOwnerPhone { get; set; }
		public bool WasSeenByOwner { get; set; }
	}
}
