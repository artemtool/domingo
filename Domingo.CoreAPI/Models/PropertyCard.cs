﻿using Domingo.CoreAPI.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class PropertyCard
	{
		public Guid Id { get; set; }
		public City City { get; set; }
		public PropertyType Type { get; set; }
		public string Title { get; set; }
		public string Address { get; set; }
		public string MainImageName { get; set; }
		public int DailyPrice { get; set; }
		public int WeeklyPrice { get; set; }
		public int Rooms { get; set; }
		public int Area { get; set; }
		public int Sleeps { get; set; }
		public int Floor { get; set; }
		public DateTime PublishDate { get; set; }
		public PropertyStatus Status { get; set; }
		public Guid OwnerId { get; set; }
	}
}
