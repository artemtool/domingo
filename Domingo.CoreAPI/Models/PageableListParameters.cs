﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class PageableListParameters
	{
		public int Page { get; set; }
		public int PerPageAmount { get; set; }
		public string OrderBy { get; set; }
		public bool IsDesc { get; set; }
	}
}
