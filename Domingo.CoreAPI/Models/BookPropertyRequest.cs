﻿using Domingo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Models
{
	public class BookPropertyRequest
	{
		public Guid PropertyId { get; set; }
		public DateTime CheckInDate { get; set; }
		public DateTime CheckOutDate { get; set; }
		public string GuestName { get; set; }
		public string GuestPhoneNumber { get; set; }
		public int GuestsAmount { get; set; }
		public string Comments { get; set; }
	}
}
