﻿using Domingo.CoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Interfaces
{
	public interface IAutocompleteManager
	{
		List<Region> GetRegions();
	}
}
