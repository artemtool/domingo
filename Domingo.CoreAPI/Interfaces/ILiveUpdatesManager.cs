﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Interfaces
{
	public interface ILiveUpdatesManager
	{
		bool IsAccessToGroupAllowed(Guid groupId, Guid userId);
		bool IsConnectedToGroup(Guid groupId, string connectionId);
		void AddHubGroupConnection(Guid groupId, string connectionId);
		void AddHubConnection(string connectionId, Guid userId);
		void DeleteHubConnection(string connectionId);
		List<string> GetUserConnectionIds(Guid userId);
	}
}
