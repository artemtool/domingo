﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Interfaces
{
	public interface IFileManager
	{
		Task<bool> SaveFormFiles(string path, List<IFormFile> files);
		bool DeleteFiles(string path, List<string> fileNames);
		List<string> GetAllFileNames(string dirPath);
	}
}
