﻿using Domingo.CoreAPI.Models;
using Domingo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Interfaces
{
	public interface IPropertyManager
	{
		List<PropertyCard> Search(SearchParameters parameters);
		Property GetById(Guid id);
		PropertyCard GetCardById(Guid id);
		User GetPropertyOwner(Guid propertyId);
		Task<Property> PublishProperty(PublishPropertyRequest request, Guid userId);
		Task<Property> UpdateProperty(UpdatePropertyRequest request, Guid userId);
		List<PropertyCard> GetPublished(PageableListParameters parameters, Guid userId);
		List<PropertyCard> GetPublishedByAddress(string adddress, Guid userId);
		bool ChangeStatus(ChangePropertyStatusRequest request);
		bool Delete(string propertyId);
		bool AreThereAnyBookings(string propertyId);
	}
}
