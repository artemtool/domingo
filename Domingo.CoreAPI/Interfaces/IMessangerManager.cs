﻿using Domingo.CoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Interfaces
{
	public interface IMessangerManager
	{
		Message SaveMessage(Message message);
		List<Message> GetMessages(FetchMessagesRequest request);
	}
}
