﻿using Domingo.CoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.CoreAPI.Interfaces
{
	public interface IBookingManager
	{
		PropertyBooking AddBooking(BookPropertyRequest request, Guid userId);
		List<PropertyBooking> GetUserBookings(PageableListParameters parameters, Guid userId);
		List<PropertyBooking> GetIncomingBookings(IncomingBookingsParameters parameters);
		List<Guid> GetAllUserConnectedBookingIds(Guid userId);
		PropertyBooking GetById(Guid id);
		void NotifyOwnerAboutNewBooking(PropertyBooking booking);
		void MarkNewPropertyBookingsAsSeen(List<PropertyBooking> bookings, Guid propertyId);
		List<NewPropertyBookings> GetNewPropertyBookings(Guid userId);
	}
}
