﻿using System;
using Newtonsoft.Json;

namespace Domingo.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastLogin { get; set; }
        public Boolean Verified { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string PhoneNumber3 { get; set; }
		public string ImageName { get; set; }
	}
}
