﻿using Domingo.Models;
using System;

namespace Domingo.Models
{
    public class RefreshToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime Expires { get; set; }
        public User User { get; set; }
    }
}
