﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.IO;
using System.Security.Cryptography;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Xml;
using System;
using System.Threading.Tasks;

namespace Domingo.Models
{
	public class TokenValidationHandler
	{
		private IConfiguration _configuration;

		public TokenValidationHandler(IConfiguration configuration)
		{
			this._configuration = configuration;
		}

		public void Configure(IServiceCollection services)
		{
			RsaSecurityKey issuerSigningKey;
			using (RSA publicRsa = RSA.Create())
			{
				var publicKeyXml = File.ReadAllText(_configuration["jwt:rsaPublicKeyXml"]);
				publicRsa.FromXmlStringExt(publicKeyXml);
				issuerSigningKey = new RsaSecurityKey(publicRsa);
			}

			services.AddAuthentication(options =>
			{
				// Identity made Cookie authentication the default.
				// However, we want JWT Bearer Auth to be the default.
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(options =>
			{
				options.RequireHttpsMetadata = false;
				options.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuer = true,
					ValidIssuer = _configuration["jwt:issuer"],

					ValidateAudience = true,
					ValidAudience = _configuration["jwt:audience"],

					ValidateLifetime = true,

					IssuerSigningKey = issuerSigningKey,
					ValidateIssuerSigningKey = true,

					ClockSkew = TimeSpan.Zero
				};

				// We have to hook the OnMessageReceived event in order to
				// allow the JWT authentication handler to read the access
				// token from the query string when a WebSocket or 
				// Server-Sent Events request comes in.
				options.Events = new JwtBearerEvents
				{
					OnMessageReceived = context =>
					{
						var accessToken = context.Request.Query["access_token"];

						// If the request is for our hub...
						var path = context.HttpContext.Request.Path;
						if (!string.IsNullOrEmpty(accessToken) &&
							(path.StartsWithSegments("/liveUpdatesHub")))
						{
							// Read the token out of the query string
							context.Token = accessToken;
						}
						return Task.CompletedTask;
					}
				};
			});
		}
	}

	public static class RsaExtension
	{
		public static void FromXmlStringExt(this RSA rsa, string xmlString)
		{
			var parameters = new RSAParameters();
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlString);

			if (xmlDoc.DocumentElement.Name.Equals("RSAKeyValue"))
			{
				foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
				{
					switch (node.Name)
					{
						case "Modulus": parameters.Modulus = Convert.FromBase64String(node.InnerText); break;
						case "Exponent": parameters.Exponent = Convert.FromBase64String(node.InnerText); break;
						case "P": parameters.P = Convert.FromBase64String(node.InnerText); break;
						case "Q": parameters.Q = Convert.FromBase64String(node.InnerText); break;
						case "DP": parameters.DP = Convert.FromBase64String(node.InnerText); break;
						case "DQ": parameters.DQ = Convert.FromBase64String(node.InnerText); break;
						case "InverseQ": parameters.InverseQ = Convert.FromBase64String(node.InnerText); break;
						case "D": parameters.D = Convert.FromBase64String(node.InnerText); break;
					}
				}
			}
			else
			{
				throw new Exception("Invalid XML RSA key.");
			}

			rsa.ImportParameters(parameters);
		}
	}
}