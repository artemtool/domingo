﻿using Domingo.Models;
using Microsoft.EntityFrameworkCore;

namespace Domingo.AuthAPI.Models
{
    public class AuthContext : DbContext
    {
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<User> Users { get; set; }

        public AuthContext(DbContextOptions<AuthContext> options):base(options)
        {

        }
    }
}
