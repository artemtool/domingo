﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.AuthAPI.Models
{
	public class RefreshTokenResult
	{
		public string AccessToken { get; set; }
		public string RefreshToken { get; set; }
	}
}
