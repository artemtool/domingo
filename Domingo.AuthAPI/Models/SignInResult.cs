﻿using Domingo.Models;

namespace Domingo.AuthAPI.Models
{
    public class SignInResult
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public User User { get; set; }
    }
}
