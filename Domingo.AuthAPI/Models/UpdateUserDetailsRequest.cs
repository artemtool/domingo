﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.AuthAPI.Models
{
	public class UpdateUserDetailsRequest
	{
		public string Name { get; set; }
		public string Surname { get; set; }
		public string Password { get; set; }
	}
}
