﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domingo.AuthAPI.Models
{
	public class UpdateUserContactsRequest
	{
		public string Email { get; set; }
		public string PhoneNumberOne { get; set; }
		public string PhoneNumberTwo { get; set; }
		public string PhoneNumberThree { get; set; }
	}
}
