﻿using Domingo.AuthAPI.Interfaces;
using Domingo.AuthAPI.Models;
using Domingo.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Domingo.AuthAPI.Managers
{
    public class AuthManager : IAuthManager
    {
        private IConfiguration _configuration;
        private AuthContext _context;
        private string _userImageStorePathTemplate;
        private string _userImageTemplateUrl;

        public AuthManager(AuthContext context, IConfiguration configuration)
        {
            this._configuration = configuration;
            this._context = context;
            this._userImageStorePathTemplate = configuration["StorePathTemplates:UserImage"];
            this._userImageTemplateUrl = configuration["RelativeUrlTemplates:UserImage"];
        }

        public ClaimsIdentity GetUserIdentity(string email, string password)
        {
            User user = this._context.Users
                .Where(u => u.Email == email)
                .FirstOrDefault();

            if (user == null)
                return null;

            string passwordHash = String.Empty;
            using (var sha = SHA256Managed.Create())
            {
                passwordHash = String.Concat(sha.ComputeHash(Encoding.UTF8.GetBytes(password))
                    .Select(item => item.ToString("x2")));
            }

            if (user.Password != passwordHash)
                return null;

            var claims = new List<Claim>
                {
                    new Claim("Id", user.Id.ToString()),
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role)
                };
            ClaimsIdentity identity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            return identity;
        }

        public User GetUserById(string id)
        {
            return this._context.Users
                .Where(u => u.Id.ToString() == id)
                .FirstOrDefault();
        }

        public string CreateAccessToken(ClaimsIdentity identity)
        {
            SigningCredentials signingCredentials;
            using (RSA privateRsa = RSA.Create())
            {
                var privateKeyXml = System.IO.File.ReadAllText(_configuration["jwt:rsaPrivateKeyXML"]);
                privateRsa.FromXmlStringExt(privateKeyXml);
                signingCredentials = new SigningCredentials(new RsaSecurityKey(privateRsa), SecurityAlgorithms.RsaSha256);
            }
            var header = new JwtHeader(signingCredentials);

			DateTime expires = DateTime.UtcNow.AddMinutes(Int32.Parse(_configuration["jwt:expiryMin"]));

            var payload = new JwtPayload(
                issuer: _configuration["jwt:issuer"],
                audience: _configuration["jwt:audience"],
                claims: identity.Claims,
                notBefore: DateTime.UtcNow,
				expires: expires
			);

            var jwt = new JwtSecurityToken(header, payload);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

			return encodedJwt;
		}

        public string CreateRefreshToken(string userId)
        {
            string refreshToken = String.Empty;
            using (var sha = SHA256Managed.Create())
            {
                refreshToken = String.Concat(sha.ComputeHash(Encoding.UTF8.GetBytes(DateTime.Now + userId))
                    .Select(item => item.ToString("x2")));
            }

            if (!String.IsNullOrEmpty(refreshToken))
            {
                var user = this.GetUserById(userId);
                var token = new RefreshToken
                {
                    Token = refreshToken,
                    Expires = DateTime.Now.AddDays(3),
                    User = user
                };
                this._context.RefreshTokens.Add(token);
                this._context.SaveChanges();
            }

            return refreshToken;
        }

        public void RemoveRefreshTokenIfExsits(string userId)
        {
            var id = new Guid(userId);
            var tokenToRemove = this._context.RefreshTokens.Where(t => t.User.Id == id).FirstOrDefault();
            if (tokenToRemove != null)
            {
                this._context.RefreshTokens.Remove(tokenToRemove);
                this._context.SaveChanges();
            }
        }

        public User CreateUser(SignUpRequest request)
        {
            string pwdHash = String.Empty;
            using (var sha = SHA256Managed.Create())
            {
                pwdHash = String.Concat(sha.ComputeHash(Encoding.UTF8.GetBytes(request.Password))
                    .Select(item => item.ToString("x2")));
            }

            User newUser = new User
            {
                Name = request.Name,
                Surname = request.Surname,
                PhoneNumber1 = request.Phone,
                Email = request.Email,
                Password = pwdHash,
                Role = "User"
            };

            this._context.Users.Add(newUser);
            this._context.SaveChanges();

            return newUser;
        }

        public async Task<User> ChangeUserImage(string userId, IFormFile file)
        {
            User user = this.GetUserById(userId);
            string pathToImagesStore = String.Format(this._userImageStorePathTemplate, userId);

            if (user.ImageName != null)
            {
                this.DeleteUserImage(userId);
            }

            bool isFileSaved = true;
            if (file != null)
            {
                try
                {
                    Directory.CreateDirectory(pathToImagesStore);

                    var filePath = Path.Combine(pathToImagesStore, file.FileName);
                    using (var fileStream = new FileStream(filePath, FileMode.CreateNew))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }
                catch (Exception ex)
                {
                    isFileSaved = false;
                    Directory.Delete(pathToImagesStore, true);
                }
            }

            if (isFileSaved)
            {
                user.ImageName = String.Format(this._userImageTemplateUrl, userId, file.FileName);
            }

            this._context.SaveChanges();

            this._context.Update(user);
            this._context.SaveChanges();

            return user;
        }

        public User DeleteUserImage(string userId)
        {
            User user = this.GetUserById(userId);
            string pathToImagesStore = String.Format(this._userImageStorePathTemplate, userId);


            if (user.ImageName != null)
            {
                bool isImageDeleted = true;
                try
                {
                    string currentImageName = Regex.Match(user.ImageName, "^.+/(.+)$")
                    .Groups[1]
                    .Value;
                    string pathToImage = String.Format("{0}\\{1}", pathToImagesStore, currentImageName);
                    File.Delete(pathToImage);
                }
                catch (Exception ex)
                {
                    isImageDeleted = false;
                }

                if (isImageDeleted)
                {
                    user.ImageName = null;

                    this._context.Update(user);
                    this._context.SaveChanges();
                }
            }

            return user;
        }

        public User UpdateUserContacts(string userId, UpdateUserContactsRequest request)
        {
            User user = this.GetUserById(userId);

            user.PhoneNumber1 = request.PhoneNumberOne != null
                ? request.PhoneNumberOne
                : user.PhoneNumber1;
            user.PhoneNumber2 = request.PhoneNumberTwo != null
                ? request.PhoneNumberTwo
                : user.PhoneNumber2;
            user.PhoneNumber3 = request.PhoneNumberThree != null
                ? request.PhoneNumberThree
                : user.PhoneNumber3;

            this._context.Update(user);
            this._context.SaveChanges();

            return user;
        }

        public User UpdateUserDetails(string userId, UpdateUserDetailsRequest request)
        {
            User user = this.GetUserById(userId);
            string pwdHash = String.Empty;
            using (var sha = SHA256Managed.Create())
            {
                pwdHash = String.Concat(sha.ComputeHash(Encoding.UTF8.GetBytes(request.Password))
                    .Select(item => item.ToString("x2")));
            }
            user.Password = pwdHash;
            user.Name = request.Name != null
                ? request.Name
                : user.Name;
            user.Surname = request.Surname != null
                ? request.Surname
                : user.Surname;

            this._context.Update(user);
            this._context.SaveChanges();

            return user;
        }

        public RefreshTokenResult RefreshAccessToken(RefreshTokenRequest request)
        {
            ClaimsPrincipal claims = this.GetClaimsFromExpiredAccessToken(request.AccessToken);
            if (claims == null)
            {
                return null;
            }
            else
            {
                string idFromExpiredAccessToken = claims.Claims
                    .Where(c => c.Type == "Id")
                    .FirstOrDefault()
                    .Value;
                RefreshToken token = this._context.RefreshTokens
                    .Where(t => t.Token == request.RefreshToken && t.User.Id.ToString() == idFromExpiredAccessToken)
                    .FirstOrDefault();

                if (token == null)
                {
                    return null;
                }
                else
                {
                    this.RemoveRefreshTokenIfExsits(idFromExpiredAccessToken);

                    ClaimsIdentity identity = new ClaimsIdentity(
                        claims.Claims,
                        "Token",
                        ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType
                     );
                    string accessToken = this.CreateAccessToken(identity);
                    string refreshToken = this.CreateRefreshToken(idFromExpiredAccessToken);

                    return new RefreshTokenResult
                    {
                        AccessToken = accessToken,
                        RefreshToken = refreshToken
                    };
                }
            }
        }

        private ClaimsPrincipal GetClaimsFromExpiredAccessToken(string token)
        {
            try
            {
                RsaSecurityKey issuerSigningKey;
                using (RSA publicRsa = RSA.Create())
                {
                    var publicKeyXml = File.ReadAllText(_configuration["jwt:rsaPublicKeyXml"]);
                    publicRsa.FromXmlStringExt(publicKeyXml);
                    issuerSigningKey = new RsaSecurityKey(publicRsa);
                }

                TokenValidationParameters validationParameters =
                new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = _configuration["jwt:issuer"],

                    ValidateAudience = true,
                    ValidAudience = _configuration["jwt:audience"],

                    ValidateLifetime = false,

                    IssuerSigningKey = issuerSigningKey,
                    ValidateIssuerSigningKey = true,

					ClockSkew = TimeSpan.Zero
				};

                SecurityToken validatedToken;
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                ClaimsPrincipal user = handler.ValidateToken(token, validationParameters, out validatedToken);

                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
