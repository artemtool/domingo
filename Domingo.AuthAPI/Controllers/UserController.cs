﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Domingo.AuthAPI.Interfaces;
using Domingo.AuthAPI.Models;
using System;
using Domingo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Domingo.AuthAPI.Controllers
{
    [Route("[controller]")]
    public class UserController : Controller
    {

        private IConfiguration _configuration;
        private IAuthManager _authManager;

        public UserController(IConfiguration configuration, IAuthManager authManager)
        {
            this._configuration = configuration;
            this._authManager = authManager;
        }

		[HttpPost("signin")]
        public IActionResult SignIn([FromBody]SignInRequest request)
        {
            var identity = this._authManager.GetUserIdentity(request.Email, request.Password);
            if (identity == null)
            {
                return Forbid();
            }
            string accessToken = this._authManager.CreateAccessToken(identity);

            string userId = identity.Claims.Where(c => c.Type.Equals("Id"))
                .FirstOrDefault().Value;
			this._authManager.RemoveRefreshTokenIfExsits(userId);
			string refreshToken = this._authManager.CreateRefreshToken(userId);
            User user = this._authManager.GetUserById(userId);

            var result = new Models.SignInResult
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                User = user
            };

            return Json(result);
        }

        [HttpPost("signup")]
        public IActionResult SignUp([FromBody]SignUpRequest request)
        {
            var user = this._authManager.CreateUser(request);

            if (user == null)
                return BadRequest();

            return Ok();
		}

		[Authorize]
		[HttpPost("signout")]
		public IActionResult SignOut()
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;
			this._authManager.RemoveRefreshTokenIfExsits(userId);

			return Ok();
		}

		[Authorize]
        [HttpPost("iam")]
        public IActionResult Iam()
        {
            string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
                .FirstOrDefault().Value;

            User user = this._authManager.GetUserById(userId);
            if (user == null)
            {
                return BadRequest();
            }

            return Json(user);
		}

		[Authorize]
		[HttpPost("changeImage")]
		public async Task<IActionResult> ChangeImage([FromForm]IFormFile image)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;
			User user = await this._authManager.ChangeUserImage(userId, image);
			
			return Ok(user);
		}

		[Authorize]
		[HttpDelete("deleteImage")]
		public async Task<IActionResult> DeleteImage()
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;
			User user = this._authManager.DeleteUserImage(userId);

			return Ok(user);
		}

		[Authorize]
		[HttpPost("updateContacts")]
		public IActionResult UpdateContacts([FromBody]UpdateUserContactsRequest request)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;
			User user = this._authManager.UpdateUserContacts(userId, request);

			return Ok(user);
		}

		[Authorize]
		[HttpPost("updateDetails")]
		public IActionResult ChangePassword([FromBody]UpdateUserDetailsRequest request)
		{
			string userId = this.User.Claims.Where(c => c.Type.Equals("Id"))
				.FirstOrDefault().Value;
			User user = this._authManager.UpdateUserDetails(userId, request);

			return Ok(user);
		}

		[HttpPost("refreshToken")]
		public IActionResult RefreshToken([FromBody]RefreshTokenRequest request)
		{
            RefreshTokenResult result = this._authManager.RefreshAccessToken(request);
            if (result == null)
            {
                return Forbid();
            } else
            {
                return Ok(result);
            }
		}
	}
}
