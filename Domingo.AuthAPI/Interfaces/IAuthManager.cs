﻿using Domingo.AuthAPI.Models;
using Domingo.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Domingo.AuthAPI.Interfaces
{
    public interface IAuthManager
    {
        ClaimsIdentity GetUserIdentity(string email, string password);
        User GetUserById(string id);
        string CreateAccessToken(ClaimsIdentity identity);
        string CreateRefreshToken(string userId);
        void RemoveRefreshTokenIfExsits(string userId);
        User CreateUser(SignUpRequest request);
		Task<User> ChangeUserImage(string userId, IFormFile file);
		User DeleteUserImage(string userId);
		User UpdateUserContacts(string userId, UpdateUserContactsRequest request);
		User UpdateUserDetails(string userId, UpdateUserDetailsRequest request);
        RefreshTokenResult RefreshAccessToken(RefreshTokenRequest request);

    }
}
